var swiper = new Swiper(".image-slider", {
  slidesPerGroup: 1,
  slidesPerView: 1,
  grabCursor: true,
  effect: "creative",
  autoplay:{
    pauseOnMouseEnter: false,
    disableOnInteraction: false
  },
  speed: 1500,
  creativeEffect: {
    prev: {
      translate: ["-120%", 0, -450],
    },
    next: {
      translate: ["120%", 0, -450],
    },
  },
  pagination: {
    el: '.main-swiper-container .swiper-pagination',
    type: 'progressbar'
  },
  navigation: {
    nextEl: '.swiper-navigation-btn .swiper-next-button',
    prevEl: '.swiper-navigation-btn .swiper-prev-button',
  }
});

let endNum = $('.total_length_slider')
  swiper && swiper.slides.length < 10 ? endNum.text(`0${swiper.slides.length }`) : endNum.text(swiper.slides.length );


let constId = swiper.realIndex + 1;
let swiperTitle = $('.swiper-title-box p');
for (let i = 0; i < swiperTitle.length; i++) {
  const elementId = parseInt($(swiperTitle[i]).data('swiper'));
  if(constId == elementId){
    $(swiperTitle[i]).addClass('show');
  }
};

swiper.on('slideChange', function() {
  var fragment = document.querySelector('.image_slider__current');
  var current = swiper.realIndex + 1;
  var idx = current < 10 ? ("0" + current) : current;
  fragment.innerHTML = idx;
  for (let i = 0; i < swiperTitle.length; i++) {
    const elementId = parseInt($(swiperTitle[i]).data('swiper'));
    if(current == elementId){
      $('.swiper-title-box p').removeClass('show');
      $(swiperTitle[i]).addClass('show');
    }
  };
});


