<footer>
        <div class="container">
            <div class="row">
                <div class="footer_container w-100">
                    <div class="footer_box">
                        <a href="index.php" class="footer_logo"><img src="img/footer_logo.svg" alt=""></a>
                        <ul>
                            <li><a href="#">Portfolio</a></li>
                            <li><a href="#">Haqqımızda</a></li>
                            <li><a href="#">Əlaqə</a></li>
                            <li><a href="#">Xidmətlər</a></li>
                            <li><a href="#">Layihələr</a></li>
                        </ul>
                    </div>
                    <div class="footer_copyright">
                        <p>© 2021. Bütün hüquqlar qorunur.</p>
                    </div>
                </div>
            </div>
        </div>
</footer>