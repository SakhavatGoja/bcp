<header>
    <div class="header-container w-100">
        <div class="container">
            <div class="row">
                <div class="header-box">
                    <ul class="header-page-lang">
                        <li><a href="#" class="az-lang active-lang">az</a></li>
                        <li><a href="#" class="ru-lang">ru</a></li>
                        <li><a href="#" class="en-lang">en</a></li>
                    </ul>
                    <div class="header-logo">
                        <a href="#">
                            <img src="./img/header-logo.svg" alt="">
                        </a>
                    </div>
                    <div class="header-menu">
                        <button class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="absolute_header">
        <div class="container">
            <div class="row">
                <div class="header_adding_container">
                    <div class="left_main_links">
                        <a href="#">Xidmətlər</a>
                        <a href="#">Portfolio</a>
                        <a href="#">Haqqımızda</a>
                        <a href="#">Layihələr</a>
                        <a href="#">Əlaqə</a>
                    </div>
                    <div class="right_main_links">
                        <div class="part_links">
                            <a href="#">BTL və Təqdimatlar</a>
                            <a href="#">Brendinq</a>
                            <a href="#">İT-Kommunikasiya</a>
                            <a href="#">Səsyazma və musiqi xidməti</a>
                        </div>
                        <div class="part_links">
                            <a href="#">UX/UI dizayn</a>
                            <a href="#">Rəqəmsal marketinq</a>
                            <a href="#">BTL və Təqdimatlar</a>
                            <a href="#">Brendinq</a>
                        </div>
                        <div class="part_links">
                            <a href="#">UX/UI dizayn</a>
                            <a href="#">Rəqəmsal marketinq</a>
                            <a href="#">BTL və Təqdimatlar</a>
                            <a href="#">Brendinq</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>